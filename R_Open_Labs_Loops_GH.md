Apply and Loops
================
Matt Jansen
April 19, 2018

### Data

<a href="http://guides.lib.unc.edu/ld.php?content_id=41513287">CH\_states\_years.zip</a>

Automating functions
--------------------

One of the most powerful features of a programming language is the ability to re-use your code or algorithm (perhaps with slight modification). We've worked with repeated tasks so far mainly through R's ability to "vectorize" and apply a function to an entire vector or group of specified vectors.

For example, recall the `paste` function, which can take individual strings or character vectors as inputs:

``` r
paste("Chapel Hill","NC",sep=", ")
```

    ## [1] "Chapel Hill, NC"

``` r
cities <- c("Chapel Hill","Durham","Raleigh","New York")
states <- c("NC","NC","NC","NY")
paste(cities,states,sep=", ")
```

    ## [1] "Chapel Hill, NC" "Durham, NC"      "Raleigh, NC"     "New York, NY"

Apply
-----

R provides a family of `apply` functions, which more generally allow us to apply functions across more complicated objects like matrices, dataframes, or lists. We'll use relatively simple objects for many of our examples today.

Three of the most common `apply` functions are:

<table class="table table-striped" style="width: auto !important; margin-left: auto; margin-right: auto;">
<thead>
<tr>
<th style="text-align:left;">
Function
</th>
<th style="text-align:left;">
Input
</th>
<th style="text-align:left;">
Output
</th>
</tr>
</thead>
<tbody>
<tr>
<td style="text-align:left;font-weight: bold;">
apply
</td>
<td style="text-align:left;">
array
</td>
<td style="text-align:left;">
vector
</td>
</tr>
<tr>
<td style="text-align:left;font-weight: bold;">
lapply
</td>
<td style="text-align:left;">
list or vector
</td>
<td style="text-align:left;">
list
</td>
</tr>
<tr>
<td style="text-align:left;font-weight: bold;">
sapply
</td>
<td style="text-align:left;">
list or vector
</td>
<td style="text-align:left;">
vector/matrix, list if necessary
</td>
</tr>
</tbody>
</table>
`apply` can be used over a matrix object. The arguments of `apply` are: 1) the object we're applying a function to 2) the 'margins' or the way we're applying the function (rows=1, columns=2) 3) and the function itself.

``` r
set.seed(6785)
L<-matrix(rnorm(30), nrow=5, ncol=6) #Create a matrix with randomly generated numbers, with dimensions 5x6.

apply(L, 2 ,sum) #sums each column and returns a vector of length 6 (the number of columns) 
```

    ## [1]  3.55243534  1.58247992  2.63525870 -0.77411822 -1.71202797  0.02518767

`apply` is a helpful function for working within matrices and arrays, but is limiting if we want to refer to lists or vectors (the funciton can get angry). For example, if we wanted to check which columns of a (mixed) dataframe were numeric, we might try:

``` r
data("iris")
apply(iris,2,is.numeric)
```

    ## Sepal.Length  Sepal.Width Petal.Length  Petal.Width      Species 
    ##        FALSE        FALSE        FALSE        FALSE        FALSE

Looks like none of our columns are numeric. Let's check `str` just to make sure.

``` r
str(iris)
```

    ## 'data.frame':    150 obs. of  5 variables:
    ##  $ Sepal.Length: num  5.1 4.9 4.7 4.6 5 5.4 4.6 5 4.4 4.9 ...
    ##  $ Sepal.Width : num  3.5 3 3.2 3.1 3.6 3.9 3.4 3.4 2.9 3.1 ...
    ##  $ Petal.Length: num  1.4 1.4 1.3 1.5 1.4 1.7 1.4 1.5 1.4 1.5 ...
    ##  $ Petal.Width : num  0.2 0.2 0.2 0.2 0.2 0.4 0.3 0.2 0.2 0.1 ...
    ##  $ Species     : Factor w/ 3 levels "setosa","versicolor",..: 1 1 1 1 1 1 1 1 1 1 ...

The problem here is that R coerces any dataframe supplied to apply into an arry with one datatype, in this case characters.

``` r
apply(iris,2,is.character)
```

    ## Sepal.Length  Sepal.Width Petal.Length  Petal.Width      Species 
    ##         TRUE         TRUE         TRUE         TRUE         TRUE

Fortunately `lapply` and `sapply` can solve this problem. (See exercises).

Let's build an example of `lapply`, and simplify with `sapply`. This also gives us a chance to learn additional functions to read in data, order data, and to split up our data for easy management.

``` r
#First, let's read in some data from GitHub about ratings of Seinfeld episodes.
seinfeld<-read.csv(url("https://raw.githubusercontent.com/audy/smalldata/master/data/seinfeld.csv"), skip=7, header=FALSE)

#We have to manually set the variable names
colnames(seinfeld)<-c("Season", "Episode", "Title", "Rating", "Votes")

#Sort our data for easier access (look at the data by season)
seinfeld<-seinfeld[order(seinfeld$Season, seinfeld$Episode),]
```

Now, let's use lapply to find the average rating per season -- which we could do by writing a loop, but `lapply` is a much more efficient approach.

First, we're going to split up our data into lists by season, over which we can use lapply:

``` r
ratings_split<-split(seinfeld$Rating, seinfeld$Season) #first object is the vector/data frame to be split, 2nd is how to split

head(ratings_split) 
```

    ## $`2`
    ##  [1] 8.0 7.9 7.8 8.5 8.7 8.5 8.0 8.0 8.4 8.3
    ## 
    ## $`3`
    ##  [1] 8.3 7.5 7.8 8.1 8.3 7.3 8.7 8.5 8.5 8.6 8.1 8.4 8.5 8.7 8.6 7.8 8.3
    ## [18] 8.6 8.7 8.6 8.0 8.5 8.6
    ## 
    ## $`4`
    ##  [1] 8.4 8.3 8.6 8.5 8.7 8.6 8.1 8.2 8.7 8.4 8.3 8.7 8.5 8.6 8.3 8.2 8.4
    ## [18] 8.5 8.4 8.7 8.7 8.4 8.5
    ## 
    ## $`5`
    ##  [1] 8.6 8.4 8.4 8.4 8.3 8.2 8.1 8.5 8.5 8.3 8.0 8.1 8.6 8.3 8.4 8.5 7.9
    ## [18] 8.0 8.5 8.7 8.5
    ## 
    ## $`6`
    ##  [1] 8.1 8.4 8.3 8.4 8.2 8.3 8.5 8.4 8.3 8.2 8.1 8.4 8.6 8.2 7.5 8.4 8.2
    ## [18] 8.5 8.3 8.4 8.1 8.5 8.2
    ## 
    ## $`7`
    ##  [1] 8.5 8.5 8.2 8.7 8.1 8.7 8.7 8.7 8.2 8.4 8.5 8.7 8.6 8.2 8.5 8.4 8.3
    ## [18] 8.0 8.2 8.5 8.4 8.7

Now, let's use lapply to find the average ranking of each season:

``` r
average_season<-lapply(ratings_split, mean)
average_season
```

    ## $`2`
    ## [1] 8.21
    ## 
    ## $`3`
    ## [1] 8.304348
    ## 
    ## $`4`
    ## [1] 8.465217
    ## 
    ## $`5`
    ## [1] 8.342857
    ## 
    ## $`6`
    ## [1] 8.282609
    ## 
    ## $`7`
    ## [1] 8.440909
    ## 
    ## $`8`
    ## [1] 8.422727
    ## 
    ## $`9`
    ## [1] 8.322727

`Sapply` functions exactly like `lapply`, but it simplifies how it returns the information into the most efficient data format (here, a vector rather than a list with elements per season, as `lapply` does):

``` r
average_season_2<-sapply(ratings_split, mean)
average_season_2
```

    ##        2        3        4        5        6        7        8        9 
    ## 8.210000 8.304348 8.465217 8.342857 8.282609 8.440909 8.422727 8.322727

Loops
-----

R provides another way to repeat a set of steps, and will probably be more familiar if you have experience with another programming language. A for loop in R has the following structure:

``` r
for(number in 1:5){
  print(number)
}
```

    ## [1] 1
    ## [1] 2
    ## [1] 3
    ## [1] 4
    ## [1] 5

There are several components here:

-   `for` tells R that this loop will iterate through each possible value supplied in the parentheses. There are other types of loops we won't cover today.

-   `(number in 1:5)` tells R to repeat the code for each value in the vector `1:5` (i.e. `c(1,2,3,4,5)`).
-   In each iteration of the loop, `number` will be set to one of the values in the vector:
    -   `number=1`
    -   `number=2`
    -   `number=3`
    -   etc.
-   `number` is a **completely arbitrary** name. As long as you're consistent, you can name it whatever you want!

-   `{print(number)}` tells R what to do each time it re-defines `number`. In this case we'll only print, but we can enter whatever code we want here, whether it depends on `number` or not. Any code that doesn't depend on `number` will execute in the same way each time.

While numbers are easy and often useful to iterate through, we can also iterate through vectors:

-   Note: This assumes that you've placed CH\_states\_years inside your working directory.

``` r
for (k in list.files("CH_states_years",full.names=TRUE)[1:10]){
  print(k)
}
```

    ## [1] "CH_states_years/CountyHealth_AK_2014.csv"
    ## [1] "CH_states_years/CountyHealth_AK_2015.csv"
    ## [1] "CH_states_years/CountyHealth_AL_2014.csv"
    ## [1] "CH_states_years/CountyHealth_AL_2015.csv"
    ## [1] "CH_states_years/CountyHealth_AR_2014.csv"
    ## [1] "CH_states_years/CountyHealth_AR_2015.csv"
    ## [1] "CH_states_years/CountyHealth_AZ_2014.csv"
    ## [1] "CH_states_years/CountyHealth_AZ_2015.csv"
    ## [1] "CH_states_years/CountyHealth_CA_2014.csv"
    ## [1] "CH_states_years/CountyHealth_CA_2015.csv"

Loops can also work with pre-existing objects alongside the iterated value.

``` r
lettersdf <- data.frame(LETTERS,ct=0)
for(k in state.abb){
  l <- substr(k,1,1)
  lettersdf[lettersdf$LETTERS==l,"ct"] <- lettersdf[lettersdf$LETTERS==l,"ct"]+1
}
```

Above we create a dataframe with each uppercase letter using the built in `LETTERS` vector. Then we iterate through the built-in `state.abb` vector of state abbreviations and use them to determine how to modify lettersdf. What does this loop actually produce?

Creating an empty object to store data strategically as the loop runs is useful in many situations. Let's write a loop to combine all of the state-year County Health subsets into one dataframe, maintaining the data encoded in the filenames.

``` r
library(stringr)
compiled <- data.frame()

for (k in list.files("CH_states_years",full.names=TRUE)){
  part <- read.csv(k)
  state <- str_split(k,"_",simplify=TRUE)[,4]
  year <- substr(str_split(k,"_",simplify=TRUE)[,5],1,4)
  part$stcode <- state
  part$yr <- year
  compiled <- rbind(compiled,part)
}
```

Exercises
---------

1.  Use `sapply` or `lapply` to select the numeric fields and create a new dataframe `seinfeldnum` with only numeric variables.

2.  Add comments to the loop generating `compiled` above, noting what's happening in each step (at whatever level you find useful).

3.  Write a for-loop that will print "Hello" ten times.

4.  Use `stringr`'s `str_replace_all` and base R's `file.rename` in a loop to rename all of the files in CH\_states\_years.

Extensions
----------

### Extending `apply` with user-defined functions

So far, we've used functions provided by R or a library we've downloaded. R also allows us to define our own functions:

``` r
div2 <- function(x){
  div  <-  sum(x)/2
  return(div)
}
div2(c(1))
```

    ## [1] 0.5

``` r
div2(1:3)
```

    ## [1] 3

-   `function` defines the upcoming bracketed code as a function; we use `<-` to name it.
-   `x` is the argument to our function. As with our loop iterator, the name is arbitrary.
-   `{...}` The bracketed code will be run whenever the function is called again.
-   `return` specifies what the function produces as an output

When we call `div2(1:3)` our function will return the sum of the vector's entries, divided by two.

Recall that `apply` family functions call a function on each element in the parent list or vector. User-defined functions let us package up other functions or complicated code to work within an `apply` function. If you've already written a loop, you can often instead package the steps as a function depdendent on your iterator and use that with apply:

``` r
for(k in 1:10){
  c <- log(k/2)
  print(c)
}
```

    ## [1] -0.6931472
    ## [1] 0
    ## [1] 0.4054651
    ## [1] 0.6931472
    ## [1] 0.9162907
    ## [1] 1.098612
    ## [1] 1.252763
    ## [1] 1.386294
    ## [1] 1.504077
    ## [1] 1.609438

``` r
sapply(1:10,function(k){
    c <- log(k/2)
    return(c)
}
)
```

    ##  [1] -0.6931472  0.0000000  0.4054651  0.6931472  0.9162907  1.0986123
    ##  [7]  1.2527630  1.3862944  1.5040774  1.6094379

Alternatively, we can pre-name the function and use it by name in both places:

``` r
myfun <- function(k){
    c<- log(k/2)
    return(c)
}

for(k in 1:10){print(myfun(k))}
```

    ## [1] -0.6931472
    ## [1] 0
    ## [1] 0.4054651
    ## [1] 0.6931472
    ## [1] 0.9162907
    ## [1] 1.098612
    ## [1] 1.252763
    ## [1] 1.386294
    ## [1] 1.504077
    ## [1] 1.609438

``` r
sapply(1:10,myfun)
```

    ##  [1] -0.6931472  0.0000000  0.4054651  0.6931472  0.9162907  1.0986123
    ##  [7]  1.2527630  1.3862944  1.5040774  1.6094379

<a href="http://gis.unc.edu/instruction/R_Open_Labs/R_Open_Lab_4_Functions.html">Read more about functions here</a>

### Extending loops and functions with `if`

The `if` function allows us to write code that will behave differently based on whether a condition is `TRUE` or `FALSE`. Its format is very similar to functions and loops.

``` r
library(ggplot2)
color="red"
data(mtcars)
if(color=="red"){
  ggplot(data=mtcars,aes(x=disp,y=mpg))+geom_point(color="red")+theme_bw()
} else {
  ggplot(data=mtcars,aes(x=disp,y=mpg))+geom_point(color="blue")+theme_bw()
}
```

![](R_Open_Labs_Loops_GH_files/figure-markdown_github/unnamed-chunk-20-1.png)

What happens if you run this code with `color="blue"`? What happens if you use `color="green"`?

We can place `if` statements inside loops or functions to change their behavior as well:

``` r
for(n in 1:25){
  if(n%%2==0){
    print(n)
  }
}
```

    ## [1] 2
    ## [1] 4
    ## [1] 6
    ## [1] 8
    ## [1] 10
    ## [1] 12
    ## [1] 14
    ## [1] 16
    ## [1] 18
    ## [1] 20
    ## [1] 22
    ## [1] 24

We can also usefully apply this to our County Health files:

#### Exercise

1.  Add comments to the code below. What does this loop print (try to figure this out without running the code first)?

``` r
for(n in 1:100){
  ind <- sum(n%%c(1:n)==0)
  if(ind==2){
    print(n)
  }
}
```

### Timing processes in R

`system.time` can be used to time the execution of given code in R. For example, this code times finding the maximum in each row of a n by n matrix as n grows from 1 to 10,000.

`apply` family functions are often faster thank loops.

``` r
speed <- function(n){
  m <- matrix(rnorm(n^2,0,1),nrow=n)
  r <- matrix(NA,nrow=n,ncol=2)
  c(system.time({r[,1] <- apply(m,2,max)})[3],system.time({for(k in 1:n){r[k,2] <- max(m[k,])}})[3])
}
  
s <- seq(1,10000,100)

res <- sapply(s,speed)
```

``` r
df <- read.csv("res0.csv") #previously generated results
ggplot(data=df,aes(x=n,y=time,colour=type))+geom_line(size=1)+theme_bw()
```

![](R_Open_Labs_Loops_GH_files/figure-markdown_github/unnamed-chunk-24-1.png)

### Read more

#### R for Data Science

<a href="http://r4ds.had.co.nz/functions.html">Functions</a> <a href="http://r4ds.had.co.nz/iteration.html#for-loops">For Loops</a>
